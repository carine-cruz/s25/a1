//solution2
db.fruits.aggregate([
	{$match:{"onSale":true}},
	{$count: "fruitsOnSale"}
])

//solution3
//used $gte based on expected output 
db.fruits.aggregate([
	{$match:{"stock":{$gte:20}}},
	{$count: "enoughStock"}
])

//solution4
db.fruits.aggregate([
	{$match:{"onSale":true},
	{$group: _id:null,
		"avg_price":{$avg: "price"}}
])

//solution5
db.fruits.aggregate([
	{$match:{"onSale":true}},
	{$group: {_id:null,
		max_price:{$max: "$price"}}}
])

//solution6
db.fruits.aggregate([
	{$match:{"onSale":true}},
	{$group: {_id:null,
		min_price:{$min: "$price"}}}
])